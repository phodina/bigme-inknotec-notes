# BIGME Inknotec Notes

This repo gathers all the information on the E-Ink Color tablet from BIGME.

> Do not update the stock OS that comes with the device otherwise the following
> guide will not be applicable!

# Backup partitions
The device contains an eMMC storage. That's formatted with GPT header. To access
the storage power down the device.

## Mtkclient
Install the tool to interact with the Mediatek
[mtk-client](https://github.com/bkerler/mtkclient).

Then run the following command, connect the device to the host with USB C cable
and power it on. The `mtk-client` should detect the device and start to run
exploit as shown below.

```
$ mtk printgpt
MTK Flash/Exploit Client V1.52 (c) B.Kerler 2018-2021

Preloader - Status: Waiting for PreLoader VCOM, please connect mobile

Port - Hint:

Power off the phone before connecting.
For brom mode, press and hold vol up, vol dwn, or all hw buttons and connect usb.
For preloader mode, don't press any hw button and connect usb.


...........

Port - Hint:

Power off the phone before connecting.
For brom mode, press and hold vol up, vol dwn, or all hw buttons and connect usb.
For preloader mode, don't press any hw button and connect usb.


..usb_class
usb_class - [LIB]: Couldn't get device configuration.
.Port - Device detected :)
Preloader - 	CPU:			MT6765(Helio P35/G35)
Preloader - 	HW version:		0x0
Preloader - 	WDT:			0x10007000
Preloader - 	Uart:			0x11002000
Preloader - 	Brom payload addr:	0x100a00
Preloader - 	DA payload addr:	0x201000
Preloader - 	CQ_DMA addr:		0x10212000
Preloader - 	Var1:			0x25
Preloader - Disabling Watchdog...
Preloader - HW code:			0x766
Preloader - Target config:		0x0
Preloader - 	SBC enabled:		False
Preloader - 	SLA enabled:		False
Preloader - 	DAA enabled:		False
Preloader - 	SWJTAG enabled:		False
Preloader - 	EPP_PARAM at 0x600 after EMMC_BOOT/SDMMC_BOOT:	False
Preloader - 	Root cert required:	False
Preloader - 	Mem read auth:		False
Preloader - 	Mem write auth:		False
Preloader - 	Cmd 0xC8 blocked:	False
Preloader - Get Target info
Preloader - 	HW subcode:		0x8a00
Preloader - 	HW Ver:			0xca00
Preloader - 	SW Ver:			0x0
Main - Device is unprotected.
DAXFlash - Uploading stage 1 from MTK_AllInOne_DA_5.2136.bin
DAXFlash - Successfully uploaded stage 1, jumping ..
Preloader - Jumping to 0x200000
Preloader - Jumping to 0x200000: ok.
DAXFlash - Successfully received DA sync
DAXFlash - DRAM config needed for : 1501003356364341
DAXFlash - Uploading stage 2...
DAXFlash - Successfully uploaded stage 2
DAXFlash - EMMC FWVer:      0x0
DAXFlash - EMMC ID:         3V6CAB
DAXFlash - EMMC CID:        150100335636434142029fe56a3088d3
DAXFlash - EMMC Boot1 Size: 0x400000
DAXFlash - EMMC Boot2 Size: 0x400000
DAXFlash - EMMC GP1 Size:   0x0
DAXFlash - EMMC GP2 Size:   0x0
DAXFlash - EMMC GP3 Size:   0x0
DAXFlash - EMMC GP4 Size:   0x0
DAXFlash - EMMC RPMB Size:  0x1000000
DAXFlash - EMMC USER Size:  0x1d1ec00000
DAXFlash - DA-CODE      : 0x666D0
DAXFlash - DA Extensions successfully added

GPT Table:
-------------
boot_para:           Offset 0x0000000000008000, Length 0x0000000000100000, Flags 0x00000000, UUID f57ad330-39c2-4488-b09b-00cb43c9ccd4, Type EFI_BASIC_DATA
para:                Offset 0x0000000000108000, Length 0x0000000000080000, Flags 0x00000000, UUID fe686d97-3544-4a41-21be-167e25b61b6f, Type EFI_BASIC_DATA
expdb:               Offset 0x0000000000188000, Length 0x0000000001400000, Flags 0x00000000, UUID 1cb143a8-b1a8-4b57-51b2-945c5119e8fe, Type EFI_BASIC_DATA
frp:                 Offset 0x0000000001588000, Length 0x0000000000100000, Flags 0x00000000, UUID 3b9e343b-cdc8-4d7f-a69f-b6812e50ab62, Type EFI_BASIC_DATA
nvcfg:               Offset 0x0000000001688000, Length 0x0000000002000000, Flags 0x00000000, UUID 5f6a2c79-6617-4b85-02ac-c2975a14d2d7, Type EFI_BASIC_DATA
nvdata:              Offset 0x0000000003688000, Length 0x0000000004000000, Flags 0x00000000, UUID 4ae2050b-5db5-4ff7-d3aa-5730534be63d, Type EFI_BASIC_DATA
md_udc:              Offset 0x0000000007688000, Length 0x000000000169a000, Flags 0x00000000, UUID 1f9b0939-e16b-4bc9-bca5-dc2ee969d801, Type EFI_BASIC_DATA
metadata:            Offset 0x0000000008d22000, Length 0x0000000002000000, Flags 0x00000000, UUID d722c721-0dee-4cb8-838a-2c63cd1393c7, Type EFI_BASIC_DATA
protect1:            Offset 0x000000000ad22000, Length 0x0000000000800000, Flags 0x00000000, UUID e02179a8-ceb5-48a9-3188-4f1c9c5a8695, Type EFI_BASIC_DATA
protect2:            Offset 0x000000000b522000, Length 0x0000000000ade000, Flags 0x00000000, UUID 84b09a81-fad2-41ac-0e89-407c24975e74, Type EFI_BASIC_DATA
seccfg:              Offset 0x000000000c000000, Length 0x0000000000800000, Flags 0x00000000, UUID e8f0a5ef-8d1b-42ea-2a9c-835cd77de363, Type EFI_BASIC_DATA
sec1:                Offset 0x000000000c800000, Length 0x0000000000200000, Flags 0x00000000, UUID d5f0e175-a6e1-4db7-c094-f82ad032950b, Type EFI_BASIC_DATA
proinfo:             Offset 0x000000000ca00000, Length 0x0000000000300000, Flags 0x00000000, UUID 1d9056e1-e139-4fca-0b8c-b75fd74d81c6, Type EFI_BASIC_DATA
nvram:               Offset 0x000000000cd00000, Length 0x0000000004000000, Flags 0x00000000, UUID 7792210b-b6a8-45d5-91ad-3361ed14c608, Type EFI_BASIC_DATA
logo:                Offset 0x0000000010d00000, Length 0x0000000000800000, Flags 0x00000000, UUID 138a6db9-1032-451d-e991-0fa38ff94fbb, Type EFI_BASIC_DATA
waveform:            Offset 0x0000000011500000, Length 0x0000000000b00000, Flags 0x00000000, UUID 756d934c-50e3-4c91-46af-02d824169ca7, Type EFI_BASIC_DATA
md1img_a:            Offset 0x0000000012000000, Length 0x0000000006400000, Flags 0x00000000, UUID a3f3c267-5521-42dd-24a7-3bdec20c7c6f, Type EFI_BASIC_DATA
spmfw_a:             Offset 0x0000000018400000, Length 0x0000000000100000, Flags 0x00000000, UUID 8c68cd2a-ccc9-4c5d-578b-34ae9b2dd481, Type EFI_BASIC_DATA
sspm_a:              Offset 0x0000000018500000, Length 0x0000000000100000, Flags 0x00000000, UUID 6a5cebf8-54a7-4b89-1d8d-c5eb140b095b, Type EFI_BASIC_DATA
gz_a:                Offset 0x0000000018600000, Length 0x0000000001000000, Flags 0x00000000, UUID a0d65bf8-e8de-4107-3494-1d318c843d37, Type EFI_BASIC_DATA
lk_a:                Offset 0x0000000019600000, Length 0x0000000000100000, Flags 0x00000000, UUID 46f0c0bb-f227-4eb6-2fb8-66408e13e36d, Type EFI_BASIC_DATA
boot_a:              Offset 0x0000000019700000, Length 0x0000000002000000, Flags 0x00000000, UUID fbc2c131-6392-4217-1eb5-548a6edb03d0, Type EFI_BASIC_DATA
vendor_boot_a:       Offset 0x000000001b700000, Length 0x0000000004000000, Flags 0x00000000, UUID e195a981-e285-4734-2580-ec323e9589d9, Type EFI_BASIC_DATA
dtbo_a:              Offset 0x000000001f700000, Length 0x0000000000800000, Flags 0x00000000, UUID e29052f8-5d3a-4e97-b5ad-5f312ce6610a, Type EFI_BASIC_DATA
tee_a:               Offset 0x000000001ff00000, Length 0x0000000000500000, Flags 0x00000000, UUID 9c3cabd7-a35d-4b45-578c-b80775426b35, Type EFI_BASIC_DATA
vbmeta_a:            Offset 0x0000000020400000, Length 0x0000000000800000, Flags 0x00000000, UUID e7099731-95a6-45a6-e5a1-1b6aba032cf1, Type EFI_BASIC_DATA
vbmeta_system_a:     Offset 0x0000000020c00000, Length 0x0000000000800000, Flags 0x00000000, UUID 8273e1ab-846f-4468-99b9-ee2ea8e50a16, Type EFI_BASIC_DATA
vbmeta_vendor_a:     Offset 0x0000000021400000, Length 0x0000000000c00000, Flags 0x00000000, UUID d26472f1-9ebc-421d-14ba-311296457c90, Type EFI_BASIC_DATA
md1img_b:            Offset 0x0000000022000000, Length 0x0000000006400000, Flags 0x00000000, UUID b72ccbe9-2055-46f4-67a1-4a069c201738, Type EFI_BASIC_DATA
spmfw_b:             Offset 0x0000000028400000, Length 0x0000000000100000, Flags 0x00000000, UUID 9c1520f3-c2c5-4b89-4282-fe4c61208a9e, Type EFI_BASIC_DATA
sspm_b:              Offset 0x0000000028500000, Length 0x0000000000100000, Flags 0x00000000, UUID 902d5f3f-434a-4de7-8889-321e88c9b8aa, Type EFI_BASIC_DATA
gz_b:                Offset 0x0000000028600000, Length 0x0000000001000000, Flags 0x00000000, UUID bece74c8-d8e2-4863-fe9b-5b0b66bb920f, Type EFI_BASIC_DATA
lk_b:                Offset 0x0000000029600000, Length 0x0000000000100000, Flags 0x00000000, UUID ff1342cf-b7be-44d5-5ea2-a435addd2702, Type EFI_BASIC_DATA
boot_b:              Offset 0x0000000029700000, Length 0x0000000002000000, Flags 0x00000000, UUID a4da8f1b-fe07-433b-cb95-84a5f23e477b, Type EFI_BASIC_DATA
vendor_boot_b:       Offset 0x000000002b700000, Length 0x0000000004000000, Flags 0x00000000, UUID c2635e15-61aa-454e-409c-ebe1bdf19b9b, Type EFI_BASIC_DATA
dtbo_b:              Offset 0x000000002f700000, Length 0x0000000000800000, Flags 0x00000000, UUID 4d2d1290-36a3-4f5d-b4af-319f8ab6dcd8, Type EFI_BASIC_DATA
tee_b:               Offset 0x000000002ff00000, Length 0x0000000000900000, Flags 0x00000000, UUID fdce12f0-a7eb-40f7-5083-960972e6cb57, Type EFI_BASIC_DATA
super:               Offset 0x0000000030800000, Length 0x0000000100000000, Flags 0x00000000, UUID 0fbbafa2-4aa9-4490-8389-5329328505fd, Type EFI_BASIC_DATA
vbmeta_b:            Offset 0x0000000130800000, Length 0x0000000000800000, Flags 0x00000000, UUID a76e4b2f-31cb-40ba-6a82-c0cb0b73c856, Type EFI_BASIC_DATA
vbmeta_system_b:     Offset 0x0000000131000000, Length 0x0000000000800000, Flags 0x00000000, UUID f54ac030-7004-4d02-8194-bbf982036807, Type EFI_BASIC_DATA
vbmeta_vendor_b:     Offset 0x0000000131800000, Length 0x0000000000800000, Flags 0x00000000, UUID c4c310e2-4a7e-77d3-1848-61e2d8bb5e86, Type EFI_BASIC_DATA
vcom:                Offset 0x0000000132000000, Length 0x0000000000800000, Flags 0x00000000, UUID 3734710f-0f13-1ab9-4c73-12a08ec50837, Type EFI_BASIC_DATA
userdata:            Offset 0x0000000132800000, Length 0x0000001be88f8000, Flags 0x00000000, UUID 85a5b02f-3773-18b3-4910-718cde95107e, Type EFI_BASIC_DATA
otp:                 Offset 0x0000001d1b0f8000, Length 0x0000000002b00000, Flags 0x00000000, UUID 6fce83a6-5273-4748-4511-c205ebb4b8ad, Type EFI_BASIC_DATA
flashinfo:           Offset 0x0000001d1dbf8000, Length 0x0000000001000000, Flags 0x00000000, UUID 3645e6a3-a7e3-19b2-4149-172c10190eef, Type EFI_BASIC_DATA

Total disk size:0x0000001d1ec00000, sectors:0x000000000e8f6000
```
Now we can backup all the partitions as well as modify any content on the eMMC
partition.

# Android
## Jailbreak
The Settings application available on the list of the applications is not the
default one found on any Android devices. It's a custom one that does not offer
developer access or any other features.

However, the default application is still packaged on the device.

In order to get into the settings we need to install the command line
application.

In this case I'll install the [FDroid](https://f-droid.org/en/) first and from
the list of available apps in this store I'll then install
[Termux](https://f-droid.org/en/packages/com.termux/).

Now just open the Termux and type the following:
```
am start com.android.settings
```

![Termux with am command](images/termux-launch-settings.png)
![Android settings](images/android-settings.png)
Android settings windows will pop-up.

## ADB
Now that we have access to the default Android settings we can now enable the
ADB by tapping the `Build number` in `About phone` tab.

After that we will get the `Developer options` under the `System` tab.

Just enable the `USB debugging` option and launch the ADB shell from the
computer.

```
$ adb shell
k65v1_64_bsp:/ $
```

![ADB](images/system-developer-options.png)

## Magisk
Since we have the ability to read and write we can just dump the `boot.img`
file and modify it offline to patch in the Magisk tools.

> This step is necessary as we don't have the root privilledges to
> dump or modify the paritions from Android directly. 

I've dumped the boot.img off the device, installed the Magisk tool on the
device. Then I pushed the  `boot.img` to the internal storage and launched
Magisk.
Then select the option to patch a file. After finished pull the file to
computer and use the `mtk-client` to write the image back to eMMC storage.

> The mtk-client has no idea which slot is active. This can be found using the
> fastboot.
```
$ mtk w boot_a boot.img
```

Now you'll get the root access on the device.

![Magisk](images/magisk-installed.png)

## Properties
```
k65v1_64_bsp:/ # cat /system/build.prop

# begin common build properties
# autogenerated by build/make/tools/buildinfo_common.sh
ro.system.build.date=Fri Oct 28 21:11:31 CST 2022
ro.system.build.date.utc=1666962691
ro.system.build.fingerprint=alps/full_k65v1_64_bsp/k65v1_64_bsp:11/RP1A.200720.011/mp1V891:user/release-keys
ro.system.build.id=RP1A.200720.011
ro.system.build.tags=release-keys
ro.system.build.type=user
ro.system.build.version.incremental=mp1V891
ro.system.build.version.release=11
ro.system.build.version.release_or_codename=11
ro.system.build.version.sdk=30
ro.product.system.brand=alps
ro.product.system.device=k65v1_64_bsp
ro.product.system.manufacturer=alps
ro.product.system.model=k65v1_64_bsp
ro.product.system.name=full_k65v1_64_bsp
# end common build properties
# begin build properties
# autogenerated by buildinfo.sh
ro.build.id=RP1A.200720.011
ro.build.display.id=XY001-XY6765CA-C.6432.R0.66.T1.1-user
ro.build.version.incremental=mp1V891
ro.build.version.sdk=30
ro.build.version.preview_sdk=0
ro.build.version.preview_sdk_fingerprint=REL
ro.build.version.codename=REL
ro.build.version.all_codenames=REL
ro.build.version.release=11
ro.build.version.release_or_codename=11
ro.build.version.security_patch=2021-01-05
ro.build.version.base_os=
ro.build.version.min_supported_target_sdk=23
ro.build.date=Fri Oct 28 21:11:31 CST 2022
ro.build.date.utc=1666962691
ro.build.type=user
ro.build.user=chichengzao
ro.build.host=ubuntu
ro.build.tags=release-keys
ro.build.locale.area=http://ereader.xrztech.com:8090
ro.build.versionCode=INKNOTE COLOR
ro.build.versionName=INKNOTE_COLOR_R_2.1.0_20221028
ro.build.flavor=full_k65v1_64_bsp-user
ro.build.system_root_image=false
# ro.product.cpu.abi and ro.product.cpu.abi2 are obsolete,
# use ro.product.cpu.abilist instead.
ro.product.cpu.abi=arm64-v8a
ro.product.cpu.abilist=arm64-v8a,armeabi-v7a,armeabi
ro.product.cpu.abilist32=armeabi-v7a,armeabi
ro.product.cpu.abilist64=arm64-v8a
ro.product.locale=zh-CN
ro.wifi.channels=
# ro.build.product is obsolete; use ro.product.device
ro.build.product=k65v1_64_bsp
# Do not try to parse description or thumbprint
ro.build.description=full_k65v1_64_bsp-user 11 RP1A.200720.011 mp1V891 release-keys
# end build properties

#
# from device/mediatek/system/common/system.prop
#
ro.product.property_source_order=odm,vendor,product,system

vendor.rild.libpath=mtk-ril.so
vendor.rild.libargs=-d /dev/ttyC0


wifi.interface=wlan0
ro.mediatek.wlan.wsc=1
ro.mediatek.wlan.p2p=1
mediatek.wlan.ctia=0


#
wifi.tethering.interface=ap0
#

ro.opengles.version=196610
# ro.kernel.qemu=1
# ro.kernel.qemu.gles=0

wifi.direct.interface=p2p0
#dalvik.vm.heapgrowthlimit=256m
#dalvik.vm.heapsize=512m

# USB MTP WHQL
ro.sys.usb.mtp.whql.enable=0

# Power off opt in IPO
sys.ipo.pwrdncap=2

ro.sys.usb.storage.type=mtp

vendor.usb.config=mtp

persist.sys.timezone=Asia/Shanghai
persist.sys.country=CN
persist.sys.language=zh

persist.global_dark_level=0
persist.global_color_enhance=100
persist.global_brightness_level=0

# USB BICR function
ro.sys.usb.bicr=no

# USB Charge only function
ro.sys.usb.charging.only=yes

# audio
ro.camera.sound.forced=0
ro.audio.silent=0

ro.zygote.preload.enable=0

# temporary enables NAV bar (soft keys)
qemu.hw.mainkeys=0

ro.kernel.zio=38,108,105,16
#ro.kernel.qemu=1
#ro.kernel.qemu.gles=0
#ro.boot.selinux=disable

# Disable dirty region for Mali
#debug.hwui.render_dirty_regions=false


# performance
ro.mtk_perf_simple_start_win=1
ro.mtk_perf_fast_start_win=1
ro.mtk_perf_response_time=1

# disable ipo for development
sys.ipo.disable=1

# identity
Build.BRAND=MTK

# Disable iorapd
ro.iorapd.enable=false

# end of device/mediatek/system/common/system.prop

#
# ADDITIONAL_BUILD_PROPERTIES
#
ro.treble.enabled=true
net.bt.name=Android
```

## Fastboot variables
To get the into the fastboot mode just execute `adb reboot-bootloader` and
you'll end up in the fastboot mode.

Then you can [list](fastboot.txt) the variables.
```
$ fastboot getvar all
```

## Android applications
The [list](android-apps.txt) of the installed applications:
```
pm list packages -f
```

## Eink panel
Some information about the eink panel can be gathered from these pseudofiles:
```
k65v1_64_bsp:/ # ls d/eink_debug/
brightness     machine_waveform  save_level   version
contrast       panel_type        sys_update   waveform
global_dither  print_level       temperature  wf_ota
global_mode    saturation        vcom
```

Also there is a sytem tool that allows to convert images into the format for the
display. This might be useful for reverse engineering.
```
/system/bin/xrz_updater --help
Usage of xrz_updater:
xrz_updater [<option> <img file>]  Update specific partition with image file
xrz_updater --update-logo  logo-verified.bin
xrz_updater --update-waveform  waveform-verified.bin
```

# Linux kernel
## Command line
```
# cat /proc/cmdline
console=tty0 console=ttyS0,921600n1 loglevel=0 vmalloc=400M slub_debug=OFZPU page_owner=on swiotlb=noforce androidboot.hardware=mt6765 maxcpus=8 loop.max_part=7 firmware_class.path=/vendor/firmware has_battery_removed=0 androidboot.boot_devices=bootdevice,soc/11230000.mmc,11230000.mmc ramoops.mem_address=0x47c90000 ramoops.mem_size=0xe0000 ramoops.pmsg_size=0x10000 ramoops.console_size=0x40000 bootopt=64S3,32N2,64N2 androidboot.selinux=permissive buildvariant=user root=/dev/ram  androidboot.slot_suffix=_a androidboot.slot=a androidboot.verifiedbootstate=orange androidboot.atm=disabled androidboot.force_normal_boot=1 androidboot.meta_log_disable=0 mtk_printk_ctrl.disable_uart=0 androidboot.serialno=INNCG229A22AL560017 androidboot.bootreason=reboot gpt=1 usb2jtag_mode=0 androidboot.dtb_idx=0 androidboot.dtbo_idx=0
```
## Kernel config
The configuration for the device can be found under [config.gz](./config.gz)

## Firmware
Here is list of the firmware files found on the device:
```
k65v1_64_bsp:/ # ls vendor/firmware/
BT_FW.cfg                     soc1_0_ram_bt_1_1_hdr.bin
WIFI_RAM_CODE_soc1_0_1_1.bin  soc1_0_ram_mcu_1_1_hdr.bin
WMT_SOC.cfg                   soc1_0_ram_wifi_1_1_hdr.bin
fm_cust.cfg                   soc3_0_ram_bt_1_1_hdr.bin
mt6631_fm_v1_coeff.bin        soc3_0_ram_bt_1a_1_hdr.bin
mt6631_fm_v1_patch.bin        soc3_0_ram_mcu_1_1_hdr.bin
rgx.fw                        soc3_0_ram_mcu_1a_1_hdr.bin
rgx.sh                        soc3_0_ram_mcu_e1_hdr.bin
soc1_0_patch_mcu_1_1_hdr.bin
```

# Disassembly
# UART connection
To debug the device it's possible to conntect to the internal UART port
(`console=ttyS0,921600n1`).
![UART pins](images/bigme-uart-pins.jpg)

# Bootlogs
[Here's](logs/boot1.log) the first of many boot logs.

# Hardware
Currently the Celluar modem is not supported. BIGME stated it will be but just
inside China.
The Hardware is certified for WiFi/BT but it misses the celluar approval from
FCC.
So even though the hardware seems to support the calls/data it might not be
possible to use it due to legal concerns

[Inknotec FCC](https://fccid.io/2A8EM-INKNOTEC)
